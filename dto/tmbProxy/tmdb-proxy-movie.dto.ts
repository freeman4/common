import { MovieResultDto } from "../tmdb/tmdb-movie.dto";

export enum MovieCheckMessageEnum {
  NotFound = 'actor not found in cast',
  Found = 'actor found in cast',
}

export class MovieCheckDto {
  message!: MovieCheckMessageEnum;

  movie!: MovieResultDto | null;

  actor_id!: number;
}