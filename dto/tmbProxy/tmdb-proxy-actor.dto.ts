import { ActorResultDto } from "../tmdb/tmdb-actor.dto";
import { MovieResultDto } from "../tmdb/tmdb-movie.dto";

export enum ActorCheckMessageEnum {
  NotFound = 'movie not found in cast',
  Found = 'movie found in cast',
}

export class ActorCheckDto {
  message!: ActorCheckMessageEnum;

  actor!: ActorResultDto | null;

  movie_id!: number;
}