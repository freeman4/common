import { MovieResultDto, WorkResultDto } from "./tmdb-movie.dto";

export class PersonDto {
  gender!: number;

  adult!: boolean;

  id!: number;

  name!: string;
}

export class ActorResultDto extends PersonDto {
  profile_path!: string | null;

  popularity!: number;

  known_for_department!: string;

  known_for!: WorkResultDto[];
}

export class ActorDto {
  page!: number;

  results!: ActorResultDto[];

  total_results!: number;

  total_pages!: number;
}

export class FilmoDto {
  id!: number;

  cast!: FilmoCastDto[];

  crew!: FilmoCrewDto[];
}

export class FilmoCastDto {
  character!: string;

  credit_id!: string;

  release_date!: string;

  vote_count!: number;

  video!: boolean;

  adult!: boolean;

  vote_average!: number;

  title!: string;

  genre_ids!: number[];

  original_language!: string;

  original_title!: string;

  popularity!: number;

  id!: number;

  backdrop_path!: string | null;

  overview!: string;

  poster_path!: string | null;

  order?: number;
}

export class FilmoCrewDto {
  id!: number;

  department!: string;

  original_language!: string;

  original_title!: string;

  job!: string;

  overview!: string;

  vote_count!: number;

  vote_average!: number;

  video!: boolean;

  poster_path!: string| null;

  backdrop_path!: string | null;

  title!: string;

  popularity!: number;

  genre_ids!: number[];

  adult!: boolean;

  release_date!: string;

  credit_id!: string;
}