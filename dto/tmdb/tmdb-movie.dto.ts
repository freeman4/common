import { PersonDto } from "./tmdb-actor.dto";

export class WorkResultDto {
  id!: number;

  genre_ids!: number[];

  media_type!: string;

  vote_average!: number;
  
  vote_count!: number;

  poster_path?: string;

  adult?: boolean;

  overview?: string;

  release_date?: string;

  original_title?: string;

  original_language?: string;

  title?: string;

  name?: string;

  backdrop_path?: string | null;

  popularity?: number;

  video?: boolean;
}

export class MovieResultDto {
    poster_path!: string | null;

    adult!: boolean;

    overview!: string;

    release_date!: string;

    genre_ids!: number[];

    id!: number;

    original_title!: string;

    original_language!: string;

    title!: string;

    backdrop_path!: string | null;

    popularity?: number;

    vote_count!: number;

    video!: boolean;

    vote_average!: number;

    media_type?: string;
}

export class MovieDto {
    page!: number;

    total_results!: number;

    total_pages!: number;

    results!: MovieResultDto[];
}

export class CastDto extends PersonDto {
  known_for_department!: string;

  original_name!: string;

  popularity!: number;

  profile_path!: string | null;

  cast_id!: number;

  character!: string;

  credit_id!: string;

  order!: number;
}

export class CrewDto extends PersonDto {
  known_for_department!: string;

  original_name!: string;

  popularity!: number;

  profile_path!: string | null;

  credit_id!: string;

  department!: string;

  job!: string;
}

export class CreditsDto {
  id!: number;

  cast!: CastDto[];

  crew!: CrewDto[];
}
